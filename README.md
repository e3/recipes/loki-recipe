# loki conda recipe

Home: https://gitlab.esss.lu.se/epics-modules/rf/loki

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS loki module
